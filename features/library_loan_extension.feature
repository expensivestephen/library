Feature: Book loan extension
  As a student
  So that I prepare for my exams
  I want to extend my book loans

  Background: Book catalog
    Given the library has the following books
      | call_no   | title                            | authors              |
      | 004.4 T21 | Programming Ruby 1.9 and 2.0     | Thomas, Hunt, Fowler |
      | 004.4 H31 | The Ruby way                     | Hal                  |
      | 004.4 B49 | Rails in action 4                | Bigg, Katz           |
      | 004.7 T46 | Agile web development with Rails | Thomas, Hansson      |
      | 004.4 R42 | Secrets of the JavaScript ninja  | Resig, Bibeault      |
      | 004.7 C90 | JavaScript : the good parts      | Crockford            |
    And I have borrowed the following books
      | username | call_no   | start_date | due_date |
      | frodob   | 004.4 H31 | 09-01-2016 | 16-01-2016  |
      | frodob   | 004.4 R42 | 09-01-2016 | 16-01-2016  |
    Given another user has reserved the book
      | username | call_no   | start_date | due_date |
      | bilbob   | 004.4 R42 | 16-01-2016 | 23-01-2016  |
    And I am browsing the "My book loans" page

  Scenario: Loan extension is possible
     When I request a loan extension of the book "The Ruby way"
     Then I should see "Extension accepted"
      And I should see "23-01-2016" as the new due date
      And I should see that the book loan has been extended once

  Scenario: Loan extension is not possible
     When I request a loan extension of the book "Secrets of the JavaScript ninja"
     Then I should see "Extension rejected"
