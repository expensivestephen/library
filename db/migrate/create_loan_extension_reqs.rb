class CreateLoanExtensionReqs < ActiveRecord::Migration
  def change
    create_table :loan_extension_reqs do |t|
      t.date :return_date
      t.string :status

      t.timestamps null: false
    end
  end
end
