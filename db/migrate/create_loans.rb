class CreateLoans < ActiveRecord::Migration
  def change
    create_table :loans do |t|
      t.date :start_date
      t.date :return_date
      t.float :cost

      t.timestamps null: false
    end
  end
end
