class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.int :call_no
      t.string :title
      t.string :authors

      t.timestamps null: false
    end
  end
end
