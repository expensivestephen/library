json.array!(@books) do |book|
  json.extract! book, :id, :call_no, :title, :authors
  json.url book_url(book, format: :json)
end
