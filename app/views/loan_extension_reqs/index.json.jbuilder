json.array!(@loan_extension_reqs) do |loan_extension_req|
  json.extract! loan_extension_req, :id, :return_date, :status
  json.url loan_extension_req_url(loan_extension_req, format: :json)
end
