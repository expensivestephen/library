json.array!(@loans) do |loan|
  json.extract! loan, :id, :start_date, :return_date, :cost
  json.url loan_url(loan, format: :json)
end
