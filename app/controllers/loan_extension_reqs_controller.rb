class LoanExtensionReqsController < ApplicationController
  before_action :set_loan_extension_req, only: [:show, :edit, :update, :destroy]

  
  def index
    @loan_extension_reqs = LoanExtensionReq.all
  end

  
  def show
  end

  
  def new
    @loan_extension_req = LoanExtensionReq.new
  end

 
  def edit
  end

  
  def create
    @loan_extension_req = LoanExtensionReq.new(loan_extension_req_params)

    respond_to do |format|
      if @loan_extension_req.save
        format.html { redirect_to @loan_extension_req, notice: 'Loan extension req was successfully created.' }
        format.json { render :show, status: :created, location: @loan_extension_req }
      else
        format.html { render :new }
        format.json { render json: @loan_extension_req.errors, status: :unprocessable_entity }
      end
    end
  end

 
  def update
    respond_to do |format|
      if @loan_extension_req.update(loan_extension_req_params)
        format.html { redirect_to @loan_extension_req, notice: 'Loan extension req was successfully updated.' }
        format.json { render :show, status: :ok, location: @loan_extension_req }
      else
        format.html { render :edit }
        format.json { render json: @loan_extension_req.errors, status: :unprocessable_entity }
      end
    end
  end

  
  def destroy
    @loan_extension_req.destroy
    respond_to do |format|
      format.html { redirect_to loan_extension_reqs_url, notice: 'Loan extension req was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
   
    def set_loan_extension_req
      @loan_extension_req = LoanExtensionReq.find(params[:id])
    end

    
    def loan_extension_req_params
      params.require(:loan_extension_req).permit(:return_date, :status)
    end
end
