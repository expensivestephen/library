class Person < ActiveRecord::Base
  has_many :loan
  has_many :books, :through => :loans
end