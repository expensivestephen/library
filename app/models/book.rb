class Book < ActiveRecord::Base
  has_many :loan
  has_many :loan_extension_reqs, :through => :loan, :source => :user

  accepts_nested_attributes_for :loan, :allow_destroy => true

  def loaned?
    loan.exists?(:return_date => nil)
  end

  def current_borrower
    if loaned?
      loan.first(:order => "out_date desc").user
    end
  end


  
  def add_loan (username)
  loan.create(:call_no   => id,
                    :username => username,
                    :start_date  => Date.current)
  end
end