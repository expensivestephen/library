require 'spec_helper'

describe Users do

  it "is not valid without a name" do
    @users = Users.create :name => nil
    @users.should_not be_valid
  end

  it "has books" do
    @book   = Book.create :author => 'Test', :title => 'Test'
    @user = User.create :name => 'Test'
    @book.update_attribute(:borrowed_to, @reader.id)
    @user.should respond_to(:books)
    @user.books.should == [@book]
  end

end