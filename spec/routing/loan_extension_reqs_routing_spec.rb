require "rails_helper"

RSpec.describe LoanExtensionReqsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/loan_extension_reqs").to route_to("loan_extension_reqs#index")
    end

    it "routes to #new" do
      expect(:get => "/loan_extension_reqs/new").to route_to("loan_extension_reqs#new")
    end

    it "routes to #show" do
      expect(:get => "/loan_extension_reqs/1").to route_to("loan_extension_reqs#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/loan_extension_reqs/1/edit").to route_to("loan_extension_reqs#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/loan_extension_reqs").to route_to("loan_extension_reqs#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/loan_extension_reqs/1").to route_to("loan_extension_reqs#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/loan_extension_reqs/1").to route_to("loan_extension_reqs#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/loan_extension_reqs/1").to route_to("loan_extension_reqs#destroy", :id => "1")
    end

  end
end
