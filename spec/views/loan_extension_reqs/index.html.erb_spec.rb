require 'rails_helper'

RSpec.describe "loan_extension_reqs/index", type: :view do
  before(:each) do
    assign(:loan_extension_reqs, [
      LoanExtensionReq.create!(
        :status => "Status"
      ),
      LoanExtensionReq.create!(
        :status => "Status"
      )
    ])
  end

  it "renders a list of loan_extension_reqs" do
    render
    assert_select "tr>td", :text => "Status".to_s, :count => 2
  end
end
