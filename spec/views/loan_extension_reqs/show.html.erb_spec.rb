require 'rails_helper'

RSpec.describe "loan_extension_reqs/show", type: :view do
  before(:each) do
    @loan_extension_req = assign(:loan_extension_req, LoanExtensionReq.create!(
      :status => "Status"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Status/)
  end
end
