require 'rails_helper'

RSpec.describe "loan_extension_reqs/edit", type: :view do
  before(:each) do
    @loan_extension_req = assign(:loan_extension_req, LoanExtensionReq.create!(
      :status => "MyString"
    ))
  end

  it "renders the edit loan_extension_req form" do
    render

    assert_select "form[action=?][method=?]", loan_extension_req_path(@loan_extension_req), "post" do

      assert_select "input#loan_extension_req_status[name=?]", "loan_extension_req[status]"
    end
  end
end
