require 'rails_helper'

RSpec.describe "loan_extension_reqs/new", type: :view do
  before(:each) do
    assign(:loan_extension_req, LoanExtensionReq.new(
      :status => "MyString"
    ))
  end

  it "renders new loan_extension_req form" do
    render

    assert_select "form[action=?][method=?]", loan_extension_reqs_path, "post" do

      assert_select "input#loan_extension_req_status[name=?]", "loan_extension_req[status]"
    end
  end
end
