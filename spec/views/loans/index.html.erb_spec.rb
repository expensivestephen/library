require 'rails_helper'

RSpec.describe "loans/index", type: :view do
  before(:each) do
    assign(:loans, [
      Loan.create!(
        :cost => 1.5
      ),
      Loan.create!(
        :cost => 1.5
      )
    ])
  end

  it "renders a list of loans" do
    render
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
  end
end
