require 'rails_helper'

RSpec.describe "books/new", type: :view do
  before(:each) do
    assign(:book, Book.new(
      :call_no => "",
      :title => "MyString",
      :authors => "MyString"
    ))
  end

  it "renders new book form" do
    render

    assert_select "form[action=?][method=?]", books_path, "post" do

      assert_select "input#book_call_no[name=?]", "book[call_no]"

      assert_select "input#book_title[name=?]", "book[title]"

      assert_select "input#book_authors[name=?]", "book[authors]"
    end
  end
end
