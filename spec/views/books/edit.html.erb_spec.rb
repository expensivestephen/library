require 'rails_helper'

RSpec.describe "books/edit", type: :view do
  before(:each) do
    @book = assign(:book, Book.create!(
      :call_no => "",
      :title => "MyString",
      :authors => "MyString"
    ))
  end

  it "renders the edit book form" do
    render

    rendered.should have_selector("form", :action => book_path(@book), :method => "post") do |form|
    end
  end
end




